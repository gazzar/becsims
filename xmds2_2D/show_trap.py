#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import matplotlib.pyplot as plt
import xmds2pyutils.hdf5_helpers as xh


a2 = xh.read_from_hdf5('trap.h5', 'trap')
xs = xh.read_from_hdf5('trap.h5', 'x')
ys = xh.read_from_hdf5('trap.h5', 'y')

im = plt.imshow(a2, cmap=plt.cm.Blues_r, origin='upper',
                extent=(xs[0], xs[-1], ys[0], ys[-1]))
plt.colorbar(im)
plt.contour(a2, 10, colors='w', origin='upper',
            extent=(xs[0], xs[-1], ys[0], ys[-1]))

plt.savefig('trap.png', dpi=75)
# plt.show()