<?xml version="1.0"?>
<!-- {timestamp} -->
<!-- Copyright (c) 2013, Gary Ruben                                          -->
<!-- Released under the Modified BSD license                                 -->
<!--                                                                         -->
<!-- Three BEC clouds evolving as per the GPE in 2+1D with the harmonic trap -->
<!-- removed at t=0. The BEC groundstate calculation is performed separately -->
<!-- and the resulting .h5 file is read in and evolved in 2+1D.              -->
<!-- This is a simulation of the experiment reported in                      -->
<!-- Scherer D R, Weiler C N, Neely T W and Anderson B P (2007). Vortex      -->
<!-- Formation by Merging of Multiple Trapped Bose-Einstein Condensates,     -->
<!-- Physical Review Letters 98(6), 110402.                                  -->
<!--                                                                         -->
<!-- The dimensionless GP equation is evolved for enough time to allow       -->
<!-- interference fringes and vortices to appear.                            -->
<!-- phiR phiI outputs the field in a format able to be read back into       -->
<!-- XmdS2 for continuing the BEC time evolution.                            -->
<!--                                                                         -->
<!-- Thanks to Graham Dennis and Joe Hope for their efforts and help with    -->
<!-- the original XmdS. For a description of XmdS2 see                       -->
<!-- Dennis G R, Hope J J, Johnsson M T (2012). XMDS2: Fast, scalable        -->
<!-- simulation of coupled stochastic partial differential equations.        -->
<!-- Computer Physics Communications 184, 201-208.                           -->

<simulation xmds-version="2">
    <!-- Global system parameters and functionality -->
    <name>time_evolve</name>
    <author>Gary Ruben</author>
    <description>
    Three BEC clouds evolving as per the GP equation in 2+1D
    </description>

    <!-- Global variables for the simulation -->
    <features>
        <benchmark />
        <fftw plan="exhaustive" />
        <openmp />
        <auto_vectorise />
        <globals>
        <![CDATA[
            const double a = {a};
            const double hbar = 1.05459e-34;
            const double m = {mass};
            const double omega = {omega};
            const double omega_z = {omega_z};
            const double l = sqrt(hbar/2.0/m/omega);                // Characteristic length scaling factor
            const double gammaC = pow(omega_z/omega/2.0/M_PI,0.5);  // 3D->2D factor - p.40 of logbook 3
            const double N = {N};
            const double alpha = a/l;                               // Dimensionaless scattering param.
            const double g = 8.0*M_PI*gammaC*N*alpha;               // Nonlinear multiplier
        ]]>
        </globals>
    </features>

    <geometry>
        <propagation_dimension>tau</propagation_dimension>
        <transverse_dimensions>
            <dimension name="x" lattice="{lattice}"  domain="(-{domain}, {domain})" />
            <dimension name="y" lattice="{lattice}"  domain="(-{domain}, {domain})" />
        </transverse_dimensions>
    </geometry>

    <!-- Field to be integrated over -->
    <vector name="wavefunction" initial_space="x y" type="complex">     <!-- A complex scalar field -->
        <components>phi</components>
        <initialisation kind="hdf5">
            <filename>field.h5</filename>
        </initialisation>
    </vector>

    <!-- This is a fairly sharp-cutoff damping vector designed to absorb any "matter"
         propagating out to the numerical lattice boundaries -->
    <vector name="vc1" dimensions="x y" type="real">
        <components>damping</components>
        <![CDATA[
            damping=100.0*(1-exp(-pow((x*x+y*y)/{damping_r}/{damping_r},10)));
        ]]>
    </vector>

    <!-- The sequence of integrations to perform -->
    <sequence>
        <integrate algorithm="ARK45" interval="{interval}" steps="{tau_steps}" tolerance="1e-6">   <!-- SI, RK45, ARK45, , ARK89RK9 -->
        <samples>1</samples>       <!-- Number of samples of associated moment groups (prob_dens
                                        and phase) over the total simulated time interval -->
        <operators>
            <operator kind="ip" constant="yes">
                <operator_names>L</operator_names>
                <![CDATA[
                    L = complex(0,-kx*kx-ky*ky);
                ]]>
            </operator>
            <integration_vectors>wavefunction</integration_vectors>
            <dependencies>vc1</dependencies>
                <![CDATA[
                    double rho2 = x*x + y*y;
                    // See p.143 of logbook for PDE
                    dphi_dtau = L[phi] - (
                             + i*g*mod2(phi)
                             + damping) * phi;
                ]]>
        </operators>
        </integrate>
    </sequence>

    <!-- The sampled output moments - we need double precision for the next timestep -->
    <!-- The output to generate -->
    <!-- <output format="hdf5" filename="next_field">    -->
    <output format="hdf5" filename="field">
        <group>
            <sampling basis="x y" initial_sample="no">
            <moments>phiR phiI</moments>
            <dependencies>wavefunction</dependencies>
            <![CDATA[
                phiR = phi.Re();
                phiI = phi.Im();
            ]]>
            </sampling>
        </group>
    </output>

</simulation>
