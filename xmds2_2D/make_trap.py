#!/usr/bin/env python

# Build trap plus lightsheet walls
# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

"""Compute the potential trap term as an xsil file for input to XmdS
Create a trap according to specified wall angles
FFT it and multiply with a circular window filter
iFFT and output as the input trapvector for xmds2
"""

from __future__ import division
import numpy as np
from numpy import array, linspace, pi, sin, exp, abs, sqrt
from numpy.fft import fft2, ifft2, fftshift, ifftshift
import matplotlib
matplotlib.use("Agg")           # Use headless backend
import matplotlib.pyplot as plt
import scipy as sp
import scipy.io as si
from PIL import Image, ImageOps, ImageDraw
from xmds2pyutils.hdf5_helpers import write_to_hdf5


def draw_lightsheet(wallT):
    def rotate(x, y, theta):
        theta *= pi/180.
        z = (x + 1j*y)*exp(1j*theta)
        return z.real, z.imag

    def translate(x, y, dx, dy):
        return x+dx, y+dy

    try:
        import aggdraw
        agg = True
        print "using aggdraw"
    except:
        agg = False
        print "WARNING: aggdraw not installed"

    epsilon = 180./12.          # Global rotation to rotate away from the lattice
    angles = [0, 120, 240]      # equiangle
    widths = [wallT, wallT, wallT]
    oX, oY = (0, 0)             # centre offset

    oX, oY = rotate(oX, oY, epsilon)
    im = Image.new("L", size=(lattice, lattice))
    if agg:
        draw = aggdraw.Draw(im)
        brush = aggdraw.Brush("white")
    else:
        draw = ImageDraw.Draw(im)

    for angle, width in zip(angles, widths):
        v = []
        for verts in [(0,-width*lattice/256.), (25./64.*lattice,0), (0,width*lattice/256.)]:
            x, y = verts
            x, y = rotate(x, y, angle+epsilon)
            x, y = translate(x, y, hl+oX, hl+oY)
            v.extend((x, y))
        if agg:
            draw.polygon(v, brush)
            draw.flush()
        else:
            draw.polygon(v, fill=255)

    return sp.misc.pilutil.fromimage(im)


# read config
import ConfigParser
cp = ConfigParser.SafeConfigParser()
cp.read("config.ini")
lattice = cp.getint("grid", "lattice")
domain = cp.getfloat("grid", "domain")

wallA = cp.getfloat("wall", "wallA")        # wall amplitude
wallTum = cp.getfloat("wall", "wallTum")    # wall thickness (um)
rho_m = cp.getfloat("wall", "rho_m")

omega = cp.getfloat("trap", "omega")
omega_z = float(eval(cp.get("trap", "omega_z").replace('M_PI', 'pi')))
m = cp.getfloat("condensate", "mass")

# build coordinates
xs = np.linspace(-domain, domain, lattice, endpoint=False)
ys = np.linspace(-domain, domain, lattice, endpoint=False)

hbar = 1.05459e-34
theta = 20.*pi/180.         # lensed light subtends this angle [radian]
wavelength = 660e-9         # 660nm laser

l = sqrt(hbar/2.0/m/omega)  # length scale [m]

side = 2*domain
print 'xy_side = %1.3g [um]' % (side*l)
scale = 2*domain/lattice                    # scale [lattice_px^-1]
wallT = wallTum/l/1e6                       # convert wall thickness to dimensionless length
wallT = wallT/scale                         # convert it to length in px
print 'wall thickness = %1.3g [px] = %1.3g [um]' % (wallT, wallT/lattice*side*l*1e6)

hl = lattice/2

# Make harmonic trap rho^2/4
a2 = array([[(i*i + j*j)/4.0 for j in ys] for i in xs])
b = draw_lightsheet(wallT)
c = fftshift(fft2(b))

px_xs, px_ys = np.ogrid[-hl:hl, -hl:hl]

# Butterworth filter
order = 2
# Build gain filter in 1D - see http://en.wikipedia.org/wiki/Butterworth_filter
butter = 1.0/sqrt(1.0 + (np.arange(lattice, dtype=float)/rho_m) ** (2*order))
c *= butter[np.hypot(px_xs, px_ys).astype(int)]
d = abs(ifft2(ifftshift(c)))

# add them
a2 += d * wallA

write_to_hdf5('trap.h5', {
        'trap': a2,
        'x': xs,
        'y': ys,
    })
