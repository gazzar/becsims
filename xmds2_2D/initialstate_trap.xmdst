<?xml version="1.0" encoding="UTF-8"?>
<!-- {timestamp} -->
<!-- Copyright (c) 2013, Gary Ruben                                          -->
<!-- Released under the Modified BSD license                                 -->
<!--                                                                         -->
<!-- BEC initial state calculation in 2D with a harmonic trap and 3-way      -->
<!-- light sheet. Based on xmds example groundstate.xmds in the examples     -->
<!-- directory. This is a simulation of the experiment reported in           -->
<!-- Scherer D R, Weiler C N, Neely T W and Anderson B P (2007). Vortex      -->
<!-- Formation by Merging of Multiple Trapped Bose-Einstein Condensates,     -->
<!-- Physical Review Letters 98(6), 110402.                                  -->
<!--                                                                         -->
<!-- The initial state is computed using the scheme whereby a Wick rotated,  -->
<!-- dimensionless GP equation is evolved in imaginary time and renormalised -->
<!-- on every iteration.                                                     -->
<!-- phiR phiI outputs the field in hdf5 format able to be read back into    -->
<!-- XmdS2 for examining the BEC time evolution.                             -->
<!--                                                                         -->
<!-- Thanks to Graham Dennis and Joe Hope for their efforts and help with    -->
<!-- the original XmdS. For a description of XmdS2 see                       -->
<!-- Dennis G R, Hope J J, Johnsson M T (2012). XMDS2: Fast, scalable        -->
<!-- simulation of coupled stochastic partial differential equations.        -->
<!-- Computer Physics Communications 184, 201-208.                           -->

<simulation xmds-version="2">
    <!-- Global system parameters and functionality -->
    <name>initialstate_trap</name>
    <author>Gary Ruben</author>
    <description>
    BEC initialstate calculation in 2D with a harmonic trap and 3-way light sheet
    </description>

    <!-- Global variables for the simulation -->
    <features>
        <benchmark />
        <fftw plan="exhaustive" />
        <openmp />
        <auto_vectorise />

        <globals>
            <![CDATA[
                const double a = {a};
                const double hbar = 1.05459e-34;
                const double m = {mass};
                const double omega = {omega};
                const double omega_z = {omega_z};
                const double l = sqrt(hbar/2.0/m/omega);                // Characteristic length scaling factor
                const double gammaC = pow(omega_z/omega/2.0/M_PI,0.5);  // 3D->2D factor - p.40 of logbook 3
                const double N = {N};
                const double alpha = a/l;                               // Dimensionaless scattering param.
                const double g = 8.0*M_PI*gammaC*N*alpha;
                const real mu = sqrt(gammaC*N*alpha);                   // p.151 of logbook 2
            ]]>
        </globals>
    </features>

    <geometry>
        <propagation_dimension>tau</propagation_dimension>
        <transverse_dimensions>
            <dimension name="x" lattice="{lattice}"  domain="(-{domain}, {domain})" />
            <dimension name="y" lattice="{lattice}"  domain="(-{domain}, {domain})" />
        </transverse_dimensions>
    </geometry>

    <vector name="wavefunction" initial_space="x y" type="complex">		<!-- A complex scalar field -->
        <components>phi</components>       	<!-- Names of the components of the vector -->
        <initialisation>
        <![CDATA[
            real rho2 = x*x + y*y;
            // See p.148 of logbook for PDE
            if (rho2 < 4.0*mu)
                phi = complex(sqrt((mu-rho2/4.0)/(2.0*M_PI*gammaC*N*alpha)), 0.0);
            else
                phi = complex(0.0, 0.0);
        ]]>
        </initialisation>
    </vector>

    <vector name="trapvector" dimensions="x y" type="real">		<!-- Holds the harmonic trap plus lightsheet -->
        <components>trap</components>     		<!-- Names of the components of the vector -->
        <initialisation kind="hdf5">
            <filename>trap.h5</filename>
        </initialisation>
    </vector>

    <computed_vector name="normalisation" dimensions="" type="real">
        <components>
            Ncalc
        </components>
        <evaluation>
        <dependencies basis="x y">wavefunction</dependencies>
        <![CDATA[
            // Calculate the current normalisation of the wave function.
            Ncalc = mod2(phi);
        ]]>
        </evaluation>
    </computed_vector>

    <!-- The sequence of integrations to perform -->
    <sequence>
        <!-- <integrate algorithm="ARK89" interval="0.1" steps="10000" tolerance="1e-8"> works -->
        <!-- <integrate algorithm="ARK45" interval="0.1" steps="10000" tolerance="1e-8"> works -->
        <!-- <integrate algorithm="RK4"   interval="0.1" steps="10000" tolerance="1e-8"> works -->
        <integrate algorithm="RK4" interval="0.1" steps="10000" tolerance="1e-8">
        <samples>1</samples>
        <filters>
            <filter>
                <dependencies>wavefunction normalisation</dependencies>
                <![CDATA[
                // Correct normalisation of the wavefunction
                phi *= sqrt(1.0/Ncalc);
                ]]>
            </filter>
        </filters>
        <operators>
            <operator kind="ip" constant="yes">
                <operator_names>L</operator_names>
                <![CDATA[
                L = complex(-kx*kx-ky*ky,0);
                ]]>
            </operator>
            <integration_vectors>wavefunction</integration_vectors>
            <dependencies>trapvector</dependencies>
                <![CDATA[
                    // See p.143 of logbook for PDE
                    dphi_dtau = L[phi] - (
                    + trap
                    + g*mod2(phi)
                    - mu) * phi;
                ]]>
        </operators>
        </integrate>
    </sequence> <!-- End integration sequence -->

    <!-- The output to generate -->
    <output format="hdf5" filename="field">
        <group>
            <sampling basis="x y" initial_sample="no">
            <moments>phiR phiI</moments>
            <dependencies>wavefunction normalisation</dependencies>
            <![CDATA[
                phi *= sqrt(1.0/Ncalc);
                phiR = phi.Re();
                phiI = phi.Im();
            ]]>
            </sampling>
        </group>
    </output>

</simulation>
