#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

from __future__ import division
import numpy as np
from numpy import pi
import ConfigParser
import os
import sys
import commands
import ast
import time
import xmds2pyutils


hbar = 1.05459e-34
a_0 = 5.291772e-11


# read config.ini values
cp = ConfigParser.SafeConfigParser()
cp.optionxform = str        # force preservation of config item case
cp.read("config.ini")

# collect config.ini values, evaluating literals wherever possible
d = {}
for section in cp.sections():
    def eval_val(val):
        try:
            return ast.literal_eval(val)
        except ValueError:
            return val

    evaled_section = {key: eval_val(val) for key, val in cp.items(section)}
    d.update(evaled_section)


# generate and inject derived values
domain = d['domain']
m = d['mass']
a = d['a']
omega = d['omega']
omega_z = eval(d['omega_z'].replace('M_PI', 'pi'))
interval = d['interval']
image_crop = d['image_crop']
l = np.sqrt(hbar/2.0/m/omega)

d.update({'omega_z': omega_z})
d.update({'N': int(eval(d['N'])) })
d.update({'interval_ms': interval/omega*1e3})
d.update({'f': omega/2.0/pi})
d.update({'f_z': omega_z/2.0/pi})
d.update({'l': l*1e6})
d.update({'xy_side': 2*domain*l*1e6})
d.update({'image_side': 2*image_crop*l*1e6})

d.update({'a_um': a*1e6})
d.update({'aa_0': a/a_0})
d.update({'alpha': a/l})

timestamp = time.asctime(time.localtime())
d.update({'timestamp': timestamp})
path = os.path.realpath('.')
d.update({'path': path})

status, output = commands.getstatusoutput('xmds2 --help')
xmds_version = output.split('\n')[-3]
d.update({'xmds_version': xmds_version})
d.update({'python_version': sys.version.replace('\n', ' ')})
d.update({'xmds2pyutils_version': xmds2pyutils.__version__})

params = """\
[provenance]
Generated on {timestamp}
Source path {path}
{xmds_version}
Python version {python_version}
xmds2pyutils version {xmds2pyutils_version}

[grid]
interval = {interval} ; = {interval_ms} [ms]
tau_steps = {tau_steps}
lattice = {lattice}
domain = {domain}
damping_r = {damping_r}

[trap]
omega = {omega} [rad s^-1]    ; f = {f} [Hz]
omega_z = {omega_z} [rad s^-1]  ; f_z = {f_z} [Hz]

[condensate]
N = {N}
m = {mass} [kg]

a = {a_um} [um]  ; = {aa_0} [a_0] ; alpha = {alpha}

l = {l} [um]
xy_side = {xy_side} [um]
image_side = {image_side} [um]\
""".format(**d)

with open('params.txt', 'w') as f:
    print >> f, params