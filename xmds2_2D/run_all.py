#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import os
import glob
import shutil
import xmds2pyutils.plot2d as xp
import xmds2pyutils.fieldnd as xf
import field_operations as fo
import ConfigParser


HDF5_FILENAME = 'field.h5'
cp = ConfigParser.SafeConfigParser()
cp.optionxform = str
cp.read("config.ini")
output_path = cp.get('output', 'path')


def run(execstr, silent=False):
    """Thin wrapper around os.system"""
    if silent:
        os.system('{} >/dev/null'.format(execstr))
    else:
        os.system(str(execstr))


def output_field(t, output_path):
    """Called at time t to output metrics and plots of the field to the output_path directory.
    """
    print 'timestep {}: '.format(t)
    domain = cp.getfloat('grid', 'domain')
    image_crop = cp.getfloat('grid', 'image_crop')

    filename = os.path.join(output_path, HDF5_FILENAME)
    run('output-field.py -s {}'.format(HDF5_FILENAME))
    run('output-field.py -do {path}/g_dens{t:04d}.png -c {crop} {h5file}'.format(
        path=output_path, t=t, crop=image_crop/domain, h5file=HDF5_FILENAME))
    run('output-field.py -po {path}/g_ph{t:04d}.png -pr 0.2 0.9 -pt 1e-3 -c {crop} {h5file}'.format(
        path=output_path, t=t, crop=image_crop/domain, h5file=HDF5_FILENAME))


def rotate_phase():
    """Modifies the phase of the field in the file specified by HDF5_FILENAME
    This just reads the file, calls rotate_phase() which applies a single
    discontinuous phase winding with steps coinciding with lightsheet walls,
    then overwrites the file with the result.
    """
    field = xf.Field(HDF5_FILENAME)
    field = fo.rotate_phase(field)
    field.write(HDF5_FILENAME)


def set_output(output_path):
    """Creates the output path and copies source scripts to allow reproduction.
    """
    print 'Writing to output path:', os.path.abspath(output_path)
    meta = os.path.join(output_path, 'meta')

    try:
        os.mkdir(output_path)
        os.mkdir(meta)
    except:
        print output_path, 'exists, overwriting.'

    xmdsts = glob.glob('*.xmdst')
    for f in ['params.txt', 'config.ini', 'run_all.py'] + xmdsts:
        shutil.copy(f, meta)


def main():
    run('./config.py initialstate_trap.xmdst config.ini')
    run('./config.py time_evolve.xmdst config.ini')

    run('./params.py')

    set_output(output_path)

    if 'field_ground.h5' in os.listdir('.'):
        print 'using field_ground.h5'
        shutil.copyfile('field_ground.h5', HDF5_FILENAME)
    else:
        run('./make_trap.py')

    if 'field_ground.h5' not in os.listdir('.'):
        run('xmds2 initialstate_trap.xmds')
        run('./initialstate_trap')
        shutil.copyfile(HDF5_FILENAME, 'field_ground.h5')
        print 'created field_ground.h5'

    run('xmds2 time_evolve.xmds')

    if 1:   # 0 to skip, 1 to rotate phase
        rotate_phase()

    t = 0
    output_field(t, output_path)

    for t in range(1, 35):
        run('./time_evolve', silent=True)
        output_field(t, output_path)


if __name__ == '__main__':
    main()
