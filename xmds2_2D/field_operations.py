#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import numpy as np
from numpy import exp, pi
import copy
import xmds2pyutils.fieldnd as xf


def rotate_phase(field):
    """Change the relative phases of the three-piece BEC segments to
    0, 2pi/3 and 4pi/3.

    Keyword arguments:
    field - a field2d.Field instance.

    Returns:
    A new field2d.Field instance.

    """
    field = copy.deepcopy(field)

    y, x = np.ogrid[field.y_domains[0]:field.y_domains[1]:field.y_lattice*1j,
                    field.x_domains[0]:field.x_domains[1]:field.x_lattice*1j]
    epsilon = pi/12.
    angles = np.arctan2(y, x) - epsilon  # get angle to each field coordinate

    # Rotate phase in 0 -> 2pi/3 and -2pi/3 -> 0 regions.
    # We write the new phase with a single assignment after modifying a local
    # copy, since the Field class doesn't support assignment to masked regions.
    phase = field.phase.copy()
    phase[np.logical_and(angles >= 0, angles < 2.*pi/3.)] += 2.*pi/3.
    phase[np.logical_and(angles >= -2.*pi/3., angles < 0)] -= 2.*pi/3.
    field.phase = phase

    return field


def find_vortices(arr, axis=0, signed=True, verbose=False):
    """Detect the vortices in a 2D or 3D array containing phase values [-pi,pi].
    Example calls:
    if x is 3D, (ax = 0, 1, or 2)
        find_vortices(x, axis=ax)
    if x is 2D
        find_vortices(x)

    Keyword arguments:
    arr - A 2d or 3d array of phase values (floats in the range [-pi,pi]).
    axis - For 3d, detects vortices in all planes parallel to axis (0, 1, 2).
    signed - True: Result array contains 1 for vortices and -1 for
                   antivortices (default).
             False: Result array contains 1 for vortices and antivortices.
    verbose - Print the slice.

    Returns:
    uint8 array filled with -1,0,1 for signed case, or 0,1 for unsigned case.

    """
    assert 2 <= len(arr.shape) <= 3
    twod = False
    if len(arr.shape) == 2:
        twod = True
        arr = arr[np.newaxis]

    xx = np.rollaxis(arr, axis)
    r = np.zeros_like(xx).astype(np.int8) - 1
    for i in range(xx.shape[0]):
        if verbose:
            print i,
        xxx = xx[i, ...]
        loop = np.concatenate(([xxx],
                               [np.roll(xxx, 1, 0)],
                               [np.roll(np.roll(xxx, 1, 0), 1, 1)],
                               [np.roll(xxx, 1, 1)],
                               [xxx]), axis=0)
        loop = np.unwrap(loop, axis=0)
        c1 = np.where(loop[0, ...]-loop[-1, ...] > -np.pi/2, 1, 0)
        c2 = np.where(loop[0, ...]-loop[-1, ...] > np.pi/2, 1, 0)
        r[i, ...] += c1 + c2

    if twod:
        r = np.rollaxis(r, 0, axis+1)[:, 1:-1, 1:-1][0]
    else:
        r = np.rollaxis(r, 0, axis+1)[1:-1, 1:-1, 1:-1]

    if not signed:
        r = np.abs(r)

    return r


if __name__ == '__main__':
    import xmds2pyutils.plot2d as xp

    phi = xf.Field('field_ground.h5')
    phi = rotate_phase(phi)
    xp.density_and_phase(phi)

    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import AxesGrid

    np.random.seed(42)
    z = np.zeros((100,100))
    z[:6,:6] = np.random.normal(size=(6,6))
    ph = np.angle(np.fft.fft2(z))
    plt.matshow(ph, cmap='gray')
    sv = find_vortices(ph, signed=True)
    plt.matshow(sv)

    z = np.zeros((20,20,20))
    z[:4,:4,:4] = np.random.normal(size=(4,4,4))
    ph = np.angle(np.fft.fftn(z))
    sv = find_vortices(ph, signed=True, verbose=True)
    fig = plt.figure(111)
    grid = AxesGrid(fig, 111, # similar to subplot(131)
                    nrows_ncols = (3, 6),
                    axes_pad = 0.05,
                    label_mode = "1")
    for i in range(18):
        im = grid[i].imshow(sv[...,i], vmin=-1, vmax=1, interpolation="nearest")
    plt.show()
