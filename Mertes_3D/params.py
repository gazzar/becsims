#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

from numpy import *
import ConfigParser

cp = ConfigParser.SafeConfigParser()
cp.read("config.ini")

interval = cp.getfloat("grid", "interval")
tau_steps = cp.getint("grid", "tau_steps")
domain = cp.getfloat("grid", "domain")
zdomain = cp.getfloat("grid", "zdomain")
lattice = cp.getint("grid", "lattice")
zlattice = cp.getint("grid", "zlattice")
damping_r = cp.getfloat("grid", "damping_r")
zdamping_r = cp.getfloat("grid", "zdamping_r")

a1 = cp.getfloat("condensate", "a1")
a2 = cp.getfloat("condensate", "a2")
a12 = cp.getfloat("condensate", "a12")
m = cp.getfloat("condensate", "mass")
N1 = cp.getint("condensate", "N1")

omega = cp.getfloat("trap", "omega")
omega_z = cp.getfloat("trap", "omega_z")
omega2 = cp.getfloat("trap", "omega2")
omega2_z = cp.getfloat("trap", "omega2_z")
zepsilon = cp.getfloat("trap", "zepsilon")

hbar = 1.05459e-34
a_0 = 5.291772e-11
l = sqrt(hbar/2.0/m/omega)
omegabar = (omega*omega*omega_z)**(1./3.)
abar = sqrt(hbar/m/omegabar)

print '[grid]'
print 'interval = %g ; = %g [ms]' % (interval, interval/omega*1e3)
print 'tau_steps = ', tau_steps
print 'lattice = ', lattice, '; zlattice = ', zlattice
print 'domain = ', domain, '; zdomain = ', zdomain
print 'x-y:z ratio = ', zdomain/domain*lattice/zlattice
print 'damping_r = ', damping_r, '; zdamping_r = ', zdamping_r
print
print '[trap]'
print 'omega = %g [rad s^-1]    ; f = %g [Hz]' % (omega, omega/2/pi)
print 'omega_z = %g [rad s^-1]  ; f_z = %g [Hz]' % (omega_z, omega_z/2/pi)
print 'omega2 = %g [rad s^-1]   ; f2 = %g [Hz]' % (omega2, omega2/2/pi)
print 'omega2_z = %g [rad s^-1] ; f2_z = %g [Hz]' % (omega2_z, omega2_z/2/pi)
print 'zepsilon = %g ; = %g [lattice divisions]' % (zepsilon*2*zdomain/zlattice, zepsilon)
print
print '[condensate]'
print 'N1 = ', N1
print 'm = %g [kg]' % m
print
print 'a11 = %g [um]  ; = %g [a_0] ; alpha11 = %g' % (a1*1e6, a1/a_0, a1/l)
print 'a22 = %g [um]  ; = %g [a_0] ; alpha22 = %g' % (a2*1e6, a2/a_0, a2/l)
print 'a12 = %g [um]  ; = %g [a_0] ; alpha12 = %g' % (a12*1e6, a12/a_0, a12/l)
gamma111 = 5.4e-42
gamma12 = 0.78e-19
gamma22 = 1.194e-19
print 'gamma111 = %g [m^6/s] ; = %g' % (gamma111, 4 * m**3 * omega**2 * gamma111 / hbar**3)
print 'gamma12 = %g [m^3/s] ; = %g' % (gamma12, m * sqrt(2*m*omega*hbar) * gamma12 / hbar**2)
print 'gamma22 = %g [m^3/s] ; = %g' % (gamma22, m * sqrt(2*m*omega*hbar) * gamma22 / hbar**2)

print

print 'l = %g [um]' % (l*1e6)
print 'xy_side = %1.3g [um]' % (2*domain*l*1e6)
print 'z_side = %1.3g [um]' % (2*zdomain*l*1e6)

mu1 = ((15*N1*a1/abar)**0.4)*omegabar/omega
print 'mu1 = %g [J] ; = %g' % (mu1*hbar*omega, mu1)

#rho for zeta=0
rho = 2*sqrt(mu1)
print 'rho at zeta=0 = %g' % rho
print 'rho*l at zeta=0 = %g [um]' % (rho*l*1e6)

#zeta for rho=0
zeta = 2*omega/omega_z*sqrt(mu1)
print 'zeta at rho=0 = %g' % zeta
print 'zeta*l at rho=0 = %g [um]' % (zeta*l*1e6)
