#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

import ConfigParser
import os
import glob
import time


def config(infile, outfile, config_file):
    """Read the xmds2 template file and populate its named patterns, specified
    like {pattern} with the corresponding named values in the file config.ini

    Keyword arguments:
    infile - an xmds2 file template with extension .xmdst, e.g. foo.xmdst
    outfile - an xmds2 file template with extension .xmds
    config_file - config file to read, e.g. config.ini

    Outputs:
    An xmds2 file with the patterns replaced named e.g. foo.xmds

    """
    cp = ConfigParser.SafeConfigParser()
    cp.optionxform = str        # force preservation of config item case
    cp.read(config_file)

    assert infile.endswith('.xmdst')
    content = open(infile).read()

    d = {}
    for section in cp.sections():
        d.update(cp.items(section))

    timestamp = 'Generated from {0} on {1}'.format(
                    infile, time.asctime(time.localtime())).ljust(71)
    d.update({'timestamp': timestamp})

    with open(outfile, 'w') as f:
        f.write(content.format(**d))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Applies template file content to an xmds2 template file')
    parser.add_argument('infile', metavar='input-file', help='input file', type=str)
    parser.add_argument('-o', '--output', metavar='output-file', help='output file', type=str)
    parser.add_argument('config', metavar='config-file', help='input config file', type=str, default='config.ini')

    args = parser.parse_args()
    if args.output is None:
        root, ext = os.path.splitext(args.infile)
        output = root + '.xmds'
    else:
        output = args.outfile
    config(args.infile, output, args.config)
