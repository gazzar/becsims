#!/usr/bin/env python

# Copyright (c) 2013, Gary Ruben
# Released under the Modified BSD license
# See LICENSE

''' Compute the potential trap term as an xsil file for input to XmdS
Create a trap according to specified wall angles
FFT it and multiply with a circular window filter
iFFT and output as an xsil or data file for input as a trap term vector
'''

import numpy as np
from numpy import array, linspace, pi, sin, exp, abs, sqrt
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import scipy as sp
import scipy.io as si
from PIL import Image, ImageOps, ImageDraw
from xmds2pyutils.hdf5_helpers import write_to_hdf5


# read config
import ConfigParser
cp = ConfigParser.SafeConfigParser()
cp.read("config.ini")
lattice = cp.getint("grid", "lattice")
zlattice = cp.getint("grid", "zlattice")
domain = cp.getfloat("grid", "domain")
zdomain = cp.getfloat("grid", "zdomain")

wallA = cp.getfloat("wall", "wallA")        # wall amplitude
wallTum = cp.getfloat("wall", "wallTum")    # wall thickness (um)
rho_m = cp.getint("wall", "rho_m")          # wall "bluriness"

omega = cp.getfloat("trap", "omega")
omega_z = cp.getfloat("trap", "omega_z")
m = cp.getfloat("condensate", "mass")

hbar = 1.05459e-34
l = sqrt(hbar/2.0/m/omega)                  # length scale [m]
l *= 1e6                                    # length scale [um]
side = 2*domain
print 'xy_side = %1.3g [um]' % (side*l)
print 'z_side = %1.3g [um]' % (2*zdomain*l)
scale = 2*domain/lattice                    # scale [lattice_px^-1]

hl = lattice/2

zepsilon = cp.getfloat("trap", "zepsilon")
# Make harmonic trap rho^2/4+zeta^2 (wz/w)^2/4
xs = linspace(-domain, domain, lattice, endpoint=False)
ys = linspace(-domain, domain, lattice, endpoint=False)
zs = linspace(-zdomain, zdomain, zlattice, endpoint=False)
a3 = array([[[(i*i+j*j)/4.0 + (((k+zepsilon)*omega_z/omega)**2)/4.0
              for k in zs] for j in ys] for i in xs])
a3slice = a3[...,zlattice/2]
plt.matshow(a3slice)
plt.jet()
plt.contour(a3slice, 15)
plt.gray()
plt.savefig('trap.png',dpi=75)
plt.close()

if 1:
    write_to_hdf5('trap.h5', {
        'trap': a3,
        'x': xs,
        'y': ys,
        'z': zs,
    })
else:
    from enthought.mayavi import mlab
    mlab.contour3d(a3)
    mlab.show()
